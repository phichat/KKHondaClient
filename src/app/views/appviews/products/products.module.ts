import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotobikeDetailComponent, AccessoryListComponent } from '.';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MotobikeDetailComponent, AccessoryListComponent]
})
export class ProductsModule { }
