export class ModelUser {
    public userId: number;
    public userFullName: string;
    public branchId: number;
};
