export class ContractModel {
    public contractId: number;
    public bookingId: number;
    public calculateId: number;
    public contractNo: string;
    public contractType: string;
    public contractDate: Date;
    public areaPayment: number;
    public contractPo: number;
    public contractGroup: number;
    public contractHire: number;
    public contractUser: number;
    public contractGurantor1: number;
    public contractGurantor2: number;
    public createdBy: number;
    public checkedBy: number;
    public approvedBy: number;
    public keeperBy: number;
    public contractStatus: number;
    public createBy: number;
    public createDate: Date;
    public updateBy: number;
    public updateDate: Date;
}
