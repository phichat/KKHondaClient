import { ContractFieldModel } from '../../models/credit';
import { FormGroup } from '@angular/forms';

export interface ContractFieldInterface {
    Item: ContractFieldModel;
    Form: FormGroup;
}
