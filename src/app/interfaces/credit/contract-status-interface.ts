import { ContractStatusModel } from '../../models/credit';
import { FormGroup } from '@angular/forms';

export interface ContractStatusInterface {
    Item: ContractStatusModel;
    Form: FormGroup;
}
