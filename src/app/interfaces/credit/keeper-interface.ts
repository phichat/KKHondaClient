import { KeeperModel } from '../../models/credit';
import { FormGroup } from '@angular/forms';

export interface KeeperInterface {
    Item: KeeperModel;
    Form: FormGroup;
}
